include:
  - networking.firewall.default

sftpd:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW
    - dport: 2200
    - proto: tcp
    - save: True
    - require:
      - sls: networking.firewall.default
