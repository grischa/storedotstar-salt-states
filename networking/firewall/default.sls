iptables-persistent:
  pkg:
    - installed

pre_flush_policy_ipv4:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: ACCEPT    
    - family: ipv4

pre_flush_policy_ipv6:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: ACCEPT
    - family: ipv6

clean_iptables_ipv4:
  iptables.flush:
    - table: filter
    - family: ipv4
    - require:
      - iptables: pre_flush_policy_ipv4

clean_iptables_ipv6:
  iptables.flush:
    - table: filter
    - family: ipv6
    - require:
      - iptables: pre_flush_policy_ipv6

ssh_ipv4:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - dport: 22
    - proto: tcp
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

ssh_ipv6:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - dport: 22
    - proto: tcp
    - family: ipv6
    - save: True
    - require:
      - iptables: clean_iptables_ipv6

local_interface_ipv4:
  iptables.append:
    - table: filter
    - chain: INPUT
    - in-interface: lo
    - jump: ACCEPT
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

local_interface_ipv6:
  iptables.append:
    - table: filter
    - chain: INPUT
    - in-interface: lo
    - jump: ACCEPT
    - family: ipv6
    - save: True
    - require:
      - iptables: clean_iptables_ipv6

established_connections_ipv4:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: ESTABLISHED,RELATED
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

established_connections_ipv6:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: ESTABLISHED,RELATED
    - family: ipv6
    - save: True
    - require:  
      - iptables: clean_iptables_ipv6

{% for host, data in salt['mine.get']('*', 'network.ipaddrs').items() -%}
{% for address in data %}
allow_{{ host }}:
  iptables.append:
    - table: filter
    - chain: INPUT
    - jump: ACCEPT
    - match: state
    - connstate: NEW,ESTABLISHED,RELATED
    - family: ipv4
    - source: {{ address }}
    - save: True
    - require:
      - iptables: clean_iptables_ipv4
    - require_in:
      - iptables: default_input_policy_drop_ipv4
      - iptables: default_output_policy_accept_ipv4
      - iptables: default_forward_policy_drop_ipv4
{% endfor %}
{% endfor %}

default_input_policy_drop_ipv4:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: DROP
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

default_output_policy_accept_ipv4:
  iptables.set_policy:
    - table: filter
    - chain: OUTPUT
    - policy: ACCEPT
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

default_forward_policy_drop_ipv4:
  iptables.set_policy:
    - table: filter
    - chain: FORWARD
    - policy: DROP
    - family: ipv4
    - save: True
    - require:
      - iptables: clean_iptables_ipv4

default_input_policy_drop_ipv6:
  iptables.set_policy:
    - table: filter
    - chain: INPUT
    - policy: DROP
    - family: ipv6
    - save: True
    - require:
      - iptables: clean_iptables_ipv6

default_output_policy_accept_ipv6:
  iptables.set_policy:
    - table: filter
    - chain: OUTPUT
    - policy: ACCEPT
    - family: ipv6
    - save: True
    - require:
      - iptables: clean_iptables_ipv6

default_forward_policy_drop_ipv6:
  iptables.set_policy:
    - table: filter
    - chain: FORWARD
    - policy: DROP
    - family: ipv6
    - save: True
    - require:
      - iptables: clean_iptables_ipv6
