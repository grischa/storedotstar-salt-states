# Sets up a hosts file with all minions present

/etc/hosts:
  file.managed:
     - source: salt://networking/templates/hosts
     - template: jinja