
# If running salt 2015.5.3, rabbitmq.py has issues detecting existing users/vhosts.
# A quick fix is to replace /usr/lib/python2.7/dist-packages/salt/modules/rabbitmq.py with
# https://raw.githubusercontent.com/saltstack/salt/27b068dfc532b6b74ad6824e160bf08348b21074/salt/modules/rabbitmq.py

rabbitmq-repo:
  pkgrepo:
  - key_url: https://www.rabbitmq.com/rabbitmq-signing-key-public.asc
  - name: deb http://www.rabbitmq.com/debian/ testing main
  - require_in:
    - pkg: rabbitmq-server
  - managed

rabbitmq-server:
  pkg:
  - installed
  service.running:
  - require:
    - pkg: rabbitmq-server

/etc/rabbitmq/rabbitmq.config:
  file:
  - contents: "[\n  {rabbit,\n    [\n    ]\n  }\n].\n"
  - require:
    - pkg: rabbitmq-server
  - managed

mytardis_vhost:
  rabbitmq_vhost:
  - name: {{ pillar['rabbitmq']['vhost'] }}
  - present
  - require:
    - pkg: rabbitmq-server
    - file: /etc/rabbitmq/rabbitmq.config

tardis_user:
  rabbitmq_user:
  - name: {{ pillar['rabbitmq']['user'] }}
  - present
  - force: true
  - password: {{ pillar['rabbitmq']['password'] }}
  - perms:
    - 'mytardis':
      - '.*'
      - '.*'
      - '.*'
  - runas: root
  - require:
    - rabbitmq_vhost: mytardis_vhost

rabbitmq_management:
  rabbitmq_plugin.enabled: []

rabbitmq_admin_user:
  rabbitmq_user:
  - name: {{ pillar['rabbitmq']['admin_user'] }}
  - present
  - force: true
  - password: {{ pillar['rabbitmq']['admin_password'] }}
  - perms:
    - '/':
      - '.*'
      - '.*'
      - '.*'
    - 'mytardis':
      - '.*'
      - '.*'
      - '.*'
  - tags:
      - administrator
  - require:
    - file: /etc/rabbitmq/rabbitmq.config
    - rabbitmq_plugin: rabbitmq_management
    - rabbitmq_vhost: mytardis_vhost

rabbitmq_guest_user:
  rabbitmq_user:
    - name: guest
    - absent
