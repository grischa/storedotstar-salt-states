refresh_pillar:
  module.run:
    - name: saltutil.refresh_pillar

flush_salt_mine:
  module.run:
    - name: mine.flush

update_salt_mine:
  module.run:
    - name: mine.update
    - require:
        - module: flush_salt_mine