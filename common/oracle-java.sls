java-repo:
  pkgrepo.managed:
    - ppa: webupd8team/java
    - require_in:
      - pkg: oracle-java8-installer

java-license-accepted:
  cmd.run:
    - name: echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && echo debconf shared/accepted-oracle-license-v1-1 seen true | /usr/bin/debconf-set-selections
    - require:
      - pkgrepo: java-repo

java:
  pkg:
    - name: oracle-java8-installer
    - latest
    - refresh: True
    - require:
      - cmd: java-license-accepted
