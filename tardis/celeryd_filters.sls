include:
  - tardis.base

/etc/supervisor/conf.d/celeryd-filters.conf:
  file:
    - managed
    - source: salt://tardis/templates/celeryd-filters.conf
    - template: jinja
    - defaults:
        ncpus: {{ grains['num_cpus'] }}
        queues: filters
    - require:
        - pkg: supervisor
