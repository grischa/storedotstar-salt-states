include:
  - tardis.base

/etc/supervisor/conf.d/gunicorn.conf:
  file:
    - managed
    - source: salt://tardis/templates/gunicorn.conf
    - require:
        - pkg: supervisor

nginx-repo:
  pkgrepo.managed:
    - ppa: nginx/stable
    - required_in:
      - pkg: nginx

ssl-cert:
  pkg:
    - installed
        
nginx:
  pkg:
    - installed
    - require:
        - pkg: ssl-cert
  service.running:
    - watch:
        - file: nginx-config

nginx-config:
  file:
    - name: /etc/nginx/sites-available/default
    - managed
    - source: salt://tardis/templates/nginx.conf
    - template: jinja
    - defaults:
        allowed_hosts:
          - {{ grains['fqdn'] }}
{% for host in salt['pillar.get']('tardis:allowed_hosts', []) %}
          - {{ host }}
{% endfor %}
{% for host, address in salt['mine.get']('G@roles:haproxy and G@deployment:'+grains['deployment'], 'network.ipaddrs', expr_form='compound').items() %}
          - {{ address[0] }}
{% endfor %}
          - {{ salt['network.interfaces']()['eth0']['inet'][0]['address'] }}
    - require:
        - pkg: nginx
