include:
  - tardis.base

/etc/supervisor/conf.d/sftpd.conf:
  file:
    - managed
    - source: salt://tardis/templates/sftpd.conf
    - template: jinja
    - defaults:
      public_ip: {{ salt['network.interfaces']()['eth0']['inet'][0]['address'] }}

sftp_server:
  supervisord.running:
    - name: sftpd
    - conf_file: /etc/supervisor/conf.d/sftpd.conf
    - update: True
    - restart: True
    - require:
        - pkg: supervisor
    - watch:
      - file: /etc/supervisor/conf.d/sftpd.conf
