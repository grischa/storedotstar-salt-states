static_files_dir:
  file:
    - name: /mnt/static_files
    - directory
    - user: mytardis
    - group: mytardis
    - mode: 755

tardis_collect_static:
  cmd.run:
    - name: cp /home/mytardis/mytardis/tardis/settings.json /home/mytardis/mytardis/tardis/settings.json.bak; tr '\n' ' ' < /home/mytardis/mytardis/tardis/settings.json.bak | sed 's/\(,\s*\)\{0,1\}"tardis.apps.publication_forms"//g' > /home/mytardis/mytardis/tardis/settings.json; su mytardis -c '/home/mytardis/virtualenvs/mytardis/bin/python mytardis.py collectstatic -c --noinput'; mv /home/mytardis/mytardis/tardis/settings.json.bak /home/mytardis/mytardis/tardis/settings.json
    - cwd: /home/mytardis/mytardis
    - user: root
    - shell: /bin/bash
    - require:
      - file: static_files_dir
