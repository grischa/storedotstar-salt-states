include:
  - tardis.users
  - tardis.filters

tardis-pkgs:
  pkg:
    - installed
    - names:
        - git
        - build-essential
        - ipython
        - libxml2-dev
        - libxslt1-dev
        - python-anyjson
        - python-billiard
        - python-bs4
        - python-crypto
        - python-dateutil
        - python-dev
        - python-feedparser
        - python-flexmock
        - python-html5lib
        - python-httplib2
        - python-magic
        - python-psycopg2
        - python-pysolr
        - python-pystache
        - python-pip
        - python-wand
        - python-yaml
        - zlib1g-dev
        - pkg-config
        - libjpeg-dev
        - libldap2-dev
        - libsasl2-dev
        - libgraphviz-dev
        - libevent-dev
        - libmagickwand5
        - python-pil # for dynamic sftp docs page
        - libfreetype6-dev # for dynamic sftp docs page
        - libhdf5-dev # for the ANSTO client - HDF5 support
        - libffi-dev  # for the ANSTO client - python requests pkg SSL support
        - libssl-dev  # for the ANSTO client - python requests pkg SSL support

/home/mytardis/mytardis:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
        - user: mytardis

mytardis-git:
  git:
    - name: {{ salt['pillar.get']('tardis:git:url', 'https://github.com/mytardis/mytardis.git') }}
    - latest
    - user: mytardis
    - target: /home/mytardis/mytardis
    - rev: {{ salt['pillar.get']('tardis:git:branch', 'develop') }}
    - submodules: True
    - force_clone: True
    - force_checkout: True
    - force_reset: True
    - require:
        - file: /home/mytardis/mytardis
        - pkg: tardis-pkgs

/home/mytardis/mytardis/vbl:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
      - user: mytardis
      - file: /home/mytardis/mytardis

vbl-git:
  git:
    - name: https://github.com/mytardis/vbl-auth.git
    - latest
    - target: /home/mytardis/mytardis/vbl
    - force_clone: True
    - force_checkout: True
    - force_reset: True
    - require:
      - git: mytardis-git
      - file: /home/mytardis/mytardis/vbl

/home/mytardis/mytardis/tardis/apps/aus_synch_app:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
      - git: mytardis-git

aus-synch-app:
  git:
    - name: https://github.com/mytardis/aus_synch_app.git
    - latest
    - target: /home/mytardis/mytardis/tardis/apps/aus_synch_app
    - force_clone: True
    - force_checkout: True
    - force_reset: True
    - require:
      - file: /home/mytardis/mytardis/tardis/apps/aus_synch_app

/home/mytardis/mytardis/tardis/apps/synch_squash_parser:
  file:
    - directory
    - user: mytardis
    - group: mytardis
    - require:
      - git: mytardis-git

synch-squash-parser:
  git:
    - name: https://github.com/grischa/synch-squash-parser.git
    - latest
    - target: /home/mytardis/mytardis/tardis/apps/synch_squash_parser
    - force_clone: True
    - force_checkout: True
    - force_reset: True
    - require:
        - file: /home/mytardis/mytardis/tardis/apps/synch_squash_parser

newest-pip:
  pip:
    - name: pip == 8.1.1
    - installed

virtualenv-pip:
  pip:
    - name: virtualenv
    - installed
    - require:
        - pip: newest-pip

mytardis-venv:
  virtualenv:
    - managed
    - name: /home/mytardis/virtualenvs/mytardis
    - requirements: /home/mytardis/mytardis/requirements.txt
    - cwd: /home/mytardis/mytardis
    - system_site_packages: True
    - no_chown: True
    - user: mytardis
    - require:
        - git: mytardis-git
        - pip: virtualenv-pip

# This state should be removed after requirements.txt is updated in git
conflicting-pip-pkgs:
  pip.removed:
    - name: pillow
    - require:
        - virtualenv: mytardis-venv

extra-pip-pkgs-requirements-txt:
  file:
    - name: /home/mytardis/extra-pip-pkgs.txt
    - managed
{% if salt['pillar.get']('tardis:extra-pip-pkgs', None) %}
    - contents: |
{% for line in salt['pillar.get']('tardis:extra-pip-pkgs') %}
        {{ line }}
{% endfor %}
{% else %}
    - contents: ""
{% endif %}

extra-pip-pkgs:
  pip.installed:
    - requirements: /home/mytardis/extra-pip-pkgs.txt
    - bin_env: /home/mytardis/virtualenvs/mytardis
    - allow_all_external: True
    - ignore_installed: True
    - force_reinstall: True
    - no_chown: True
    - user: mytardis
    - require:
        - virtualenv: mytardis-venv
        - file: extra-pip-pkgs-requirements-txt

{% set tardis_db_user = salt['pillar.get']('tardis:db_user', 'mytardis-'+grains['deployment']) %}
{% set tardis_db_password = salt['pillar.get']('tardis:db_password', 'myt4rd15') %}
{% set tardis_db_name = salt['pillar.get']('tardis:db_name', 'mytardis-'+grains['deployment']) %}

{% if salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound') %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_pgpool', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% else %}
{% set tardis_db_host = salt['mine.get']('G@roles:postgresql_server_master', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% endif %}

{% if salt['mine.get']('G@roles:elasticsearch_loadbalancer', 'network.ipaddrs', expr_form='compound') %}
{% set elasticsearch_host = salt['mine.get']('G@roles:elasticsearch_loadbalancer', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% elif salt['mine.get']('G@roles:elasticsearch', 'network.ipaddrs', expr_form='compound') %}
{% set elasticsearch_host = salt['mine.get']('G@roles:elasticsearch', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% else %}
{% set elasticsearch_host = None %}
{% endif %}

{% set rabbitmq_user = pillar['rabbitmq']['user'] %}
{% set rabbitmq_password = pillar['rabbitmq']['password'] %}
{% set rabbitmq_vhost = pillar['rabbitmq']['vhost'] %}
{% set rabbitmq_ip = salt['mine.get']('G@roles:rabbitmq and G@deployment:'+grains['deployment'], 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}

tardis_settings_json:
  file.serialize:
    - name: /home/mytardis/mytardis/tardis/settings.json
    - dataset_pillar: tardis
    - formatter: json
    - user: root
    - group: mytardis

tardis_settings_py:
  file:
    - name: /home/mytardis/mytardis/tardis/settings.py
    - managed
    - source: salt://tardis/templates/settings.py
    - template: jinja
    - user: root
    - group: mytardis
    - mode: 650
    - defaults:
        debug: {{ pillar.get('tardis:settings:debug', grains['deployment'] != 'prod') }}
        tardis_db: {{ tardis_db_name }}
        tardis_db_user: {{ tardis_db_user }}
        tardis_db_password: {{ tardis_db_password }}
        tardis_db_host: {{ tardis_db_host }}
        rabbitmq_user: {{ rabbitmq_user }}
        rabbitmq_password: {{ rabbitmq_password }}
        rabbitmq_vhost: {{ rabbitmq_vhost }}
        rabbitmq_ip: {{ rabbitmq_ip }}
        elasticsearch_host: {{ elasticsearch_host }}
    - require:
        - git: mytardis-git
        - file: tardis_settings_json

{% if salt['pillar.get']('tardis:settings:custom_about_page_template', None) %}
custom_about_page_template:
  file.managed:
    - user: mytardis
    - group: mytardis
    - mode: 660
    - name: /home/mytardis/mytardis/tardis/tardis_portal/templates/tardis_portal/{{ salt['pillar.get']('tardis:settings:custom_about_page_template') }}
    - contents_pillar: tardis:settings:custom_about_page_content
    - require:
        - file: tardis_settings_py
{% endif %}

supervisor:
  pkg:
    - installed
  service.running:
    - watch:
        - file: /etc/supervisor/conf.d/*
        - file: tardis_settings_py
        - pip: extra-pip-pkgs
        - pip: conflicting-pip-pkgs
    - require:
        - pkg: tardis-pkgs
        - file: tardis_settings_py
        - file: /etc/supervisor/conf.d/*
        - pip: extra-pip-pkgs
        - pip: conflicting-pip-pkgs

# create temporary folder for ImageMagick temporary filters
{% set magick_tmpdir = salt['pillar.get']('tardis:magick_tmpdir', '/tmp') %}
{% if magick_tmpdir != '/tmp' and magick_tmpdir != '' and magick_tmpdir != '/' %}
{{ magick_tmpdir }}:
  file:
    - directory
    - user: mytardis
    - group: mytardis

# The default installation disables the cron job that comes with tmpreaper
# Leaving it disabled and only running it on the safe magick tmp dir
tmpreaper:
  pkg:
    - installed
  schedule:
    - present
    - function: cmd.run
    - hours: 1
    - job_args: 'tmpreaper 4h {{ magick_tmpdir }}'
{% else %}
{% set magick_tmpdir = '/tmp' %}
{% endif %}

# adding global config file, adds/overrides individual settings in default one
supervisord.conf:
  file.managed:
    - name: /etc/supervisor/conf.d/supervisord.conf
    - source: salt://tardis/templates/supervisord.conf
    - template: jinja
    - defaults:
      magick_tmpdir: {{ magick_tmpdir }}
    - require:
      - pkg: supervisor
