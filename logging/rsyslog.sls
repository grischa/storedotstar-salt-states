{% if salt['mine.get']('G@roles:graylog', 'network.ipaddrs', expr_form='compound') %}
rsyslog-gnutls:
  pkg:
    - installed

rsyslog:
  service.running:
    - enable: true
    - watch:
      - file: /etc/rsyslog.d/90-graylog2.conf
    - require:
        - pkg: rsyslog-gnutls

# With TLS
{% if salt['pillar.get']('rsyslog:ca-cert', None) %}
/etc/rsyslog_ca.pem:
  file:
    - managed
    - contents_pillar: rsyslog:ca-cert
    - mode: 644
    - user: root
    - group: root

/etc/rsyslog.d/90-graylog2.conf:
  file:
    - managed
    - source: salt://logging/templates/rsyslog_tls.conf
    - template: jinja
    - defaults:
        graylog_server: {{ salt['mine.get']('G@roles:graylog', 'network.ipaddrs', expr_form='compound').items()[0][1][0] }}
    - require:
        - file: /etc/rsyslog_ca.pem

{% else %}
# Without TLS
/etc/rsyslog.d/90-graylog2.conf:
  file:
    - managed
    - source: salt://logging/templates/rsyslog.conf
    - template: jinja
    - defaults:
        graylog_server: {{ salt['mine.get']('G@roles:graylog', 'network.ipaddrs', expr_form='compound').items()[0][1][0] }}
{% endif %}

{% endif %}