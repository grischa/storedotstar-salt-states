include:
  - ssl

trusty-backports:
  pkgrepo.managed:
    - humanname: Trusty Backports
    - name: deb http://archive.ubuntu.com/ubuntu trusty-backports main universe
    - file: /etc/apt/sources.list.d/backports.list
    - require_in:
        - pkg: haproxy

haproxy:
  pkg:
    - latest
    - refresh: True
    - fromrepo: trusty-backports
    - require:
        - cmd: combined_cert_and_key
  service.running:
    - watch:
        - file: /etc/haproxy/haproxy.cfg
        - user: haproxy_group_membership
        - cmd: combined_cert_and_key

haproxy_group_membership:
  user:
    - name: haproxy
    - groups:
        - haproxy
        - ssl-cert
    - present
    - require:
        - pkg: ssl-cert
        - pkg: haproxy

combined_cert_and_key:
  cmd.wait:
    - name: cat /etc/ssl/certs/mytardis.pem /etc/ssl/private/mytardis.key > /etc/ssl/private/haproxy_combined.pem && chown root:ssl-cert /etc/ssl/private/haproxy_combined.pem && chmod 640 /etc/ssl/private/haproxy_combined.pem
    - watch:
        - file: cert_file
        - file: priv_key_file
    - require:
        - file: cert_file
        - file: priv_key_file
        - sls: ssl

/etc/haproxy/haproxy.cfg:
  file:
    - managed
{% if 'elasticsearch_loadbalancer' in salt['grains.get']('roles', []) %}
    - source: salt://haproxy/templates/haproxy-elasticsearch.cfg
{% else %}
    - source: salt://haproxy/templates/haproxy-web.cfg
{% endif %}
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - defaults:
        public_ip: {{ salt['network.interfaces']()['eth0']['inet'][0]['address'] }}
