include:
  - common.oracle-java

/var/lib/tardis_storage/elasticsearch:
  file.directory:
    - user: root
    - group: root
    - mode: 775
    - makedirs: True
    - require:
        - sls: storage

/elasticsearch:
  file.symlink:
    - target: /var/lib/tardis_storage/elasticsearch
    - require:
        - file: /var/lib/tardis_storage/elasticsearch

elasticsearch_1.7_data_dir:
  file:
    - name: /elasticsearch/data
    - directory
    - user: elasticsearch
    - group: root
    - makedirs: true
    - require:
      - pkg: elasticsearch-1.7
      - file: /elasticsearch
  
elasticsearch-1.7-repo:
  pkgrepo.managed:
    - key_url: https://packages.elastic.co/GPG-KEY-elasticsearch
    - name: deb http://packages.elastic.co/elasticsearch/1.7/debian stable main
    - require_in:
      - pkg: elasticsearch-1.7

elasticsearch-1.7:
  pkg:
    - name: elasticsearch
    - latest
    - refresh: True
    - require:
      - pkg: java
  service.running:
    - name: elasticsearch
    - enable: true
    - watch:
      - file: elasticsearch-config

elasticsearch-config:
  file:
    - name: /etc/elasticsearch/elasticsearch.yml
    - managed
    - source: salt://database/templates/elasticsearch.yml
    - template: jinja
