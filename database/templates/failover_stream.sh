#!/bin/sh
# Failover command for streaming replication.
# Arguments: $1: new master hostname. $2: path to
# trigger file.

new_master=$1
data_dir=$2
trigger_file=$3

# Do nothing if standby goes down.
failed_is_slave=$(echo "if [ -f $data_dir/recovery.conf ]; then echo 0; else echo 1; fi" | ssh -T "$new_master")
if [ "$failed_is_slave" = 1 ]; then
  exit 0;
fi

if [ "$new_master" = -1 ]; then
  exit 0;
fi

# Create the trigger file.
ssh -T "$new_master" touch "$trigger_file"

exit 0;

