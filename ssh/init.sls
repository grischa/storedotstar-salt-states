{% if pillar['root_id_rsa'] is defined %}
/root/.ssh:
  file:
    - directory
    - mode: 700
    - user: root
    - group: root

/root/.ssh/id_rsa:
  file.managed:
    - contents_pillar: root_id_rsa
    - mode: 600
    - user: root
    - group: root
{% endif %}

{% for known_host in pillar.get('known_hosts', []) %}
known_host_{{ known_host['host'] }}:
  ssh_known_hosts.present:
    - user: root
    - name: {{ known_host['host'] }}
    - fingerprint: {{ known_host['fingerprint'] }}
    - enc: {{ known_host['enc'] }}
{% endfor %}
