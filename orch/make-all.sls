refresh_pillars:
  salt.state:
    - tgt: '*'
    - sls:
      - common.salt_refresh
    - expect_minions: True
    - timeout: 3600
  
setup_database_master:
  salt.state:
    - tgt: 'roles:postgresql_server_master'
    - tgt_type: grain
    - highstate: True
    - expect_minions: True
    - timeout: 3600
    - require:
        - salt: refresh_pillars

setup_database_slave:
  salt.state:
    - tgt: 'roles:postgresql_server_slave'
    - tgt_type: grain
    - highstate: True
    - expect_minions: True
    - timeout: 3600
    - require:
        - salt: setup_database_master
        - salt: refresh_pillars

setup_database_loadbalancer:
  salt.state:
    - tgt: 'roles:postgresql_pgpool'
    - tgt_type: grain
    - highstate: True
    - expect_minions: True
    - timeout: 3600
    - require:
      - salt: setup_database_master
      - salt: setup_database_slave
      - salt: refresh_pillars

setup_web_workers:
  salt.state:
    - tgt: 'roles:www'
    - tgt_type: grain
    - highstate: True
    - expect_minions: True
    - timeout: 3600
    - require:
        - salt: refresh_pillars

{% if salt['pillar.get']('tardis:force_restore', False) %}
drop_db:
  salt.state:
    - tgt: 'roles:www'
    - tgt_type: grain
    - sls:
      - tardis.db-drop
    - expect_minions: True
    - timeout: 3600
    - require_in: migrate_db
    - require:
      - salt: setup_web_workers
      - salt: refresh_pillars
{% endif %}
      
migrate_db:
  salt.state:
    - tgt: 'roles:www'
    - tgt_type: grain
    - sls:
      - tardis.db-restore
      - tardis.db-migrate
    - expect_minions: True
    - timeout: 3600
    - require:
      - salt: setup_web_workers
      - salt: setup_database_loadbalancer
      - salt: refresh_pillars

collect_static_files:
  salt.state:
    - tgt: 'roles:www'
    - tgt_type: grain
    - sls:
      - tardis.collectstatic
    - expect_minions: True
    - timeout: 3600
    - require:
      - salt: migrate_db
      - salt: refresh_pillars

setup_remaining_services:
  salt.state:
    - tgt: '*'
    - highstate: True
    - expect_minions: True
    - timeout: 3600
    - require:
      - salt: setup_web_workers
      - salt: migrate_db
      - salt: collect_static_files
      - salt: refresh_pillars
