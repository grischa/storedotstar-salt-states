
{% if 'rabbitmq' in salt['grains.get']('roles', []) %}
{% set rabbitmq_admin_user = pillar['rabbitmq']['admin_user'] %}
{% set rabbitmq_admin_password = pillar['rabbitmq']['admin_password'] %}
{% set rabbitmq_vhost = pillar['rabbitmq']['vhost'] %}
{% set rabbitmq_ip = salt['mine.get']('G@roles:rabbitmq and G@deployment:'+grains['deployment'], 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}
{% endif %}
{% set nagios_host = salt['mine.get']('G@roles:nagios_core', 'network.ipaddrs', expr_form='compound').items()[0][1][0] %}

/etc/nagios/nrpe.cfg:
  file.managed:
    - source: salt://nagios/templates/nrpe.cfg
    - template: jinja
    - makedirs: True
    - mode: 644
    - user: root
    - group: root
    - defaults:
        nagios_host: {{ nagios_host }}
    {% if 'rabbitmq' in salt['grains.get']('roles', []) %}
        rabbitmq_admin_user: {{ rabbitmq_admin_user }}
        rabbitmq_admin_password: {{ rabbitmq_admin_password }}
        rabbitmq_vhost: {{ rabbitmq_vhost }}
        rabbitmq_ip: {{ rabbitmq_ip }}
    {% endif %}

nagios-nrpe-server:
  pkg:
    - installed
  service:
    - running
    - watch:
        - file: /etc/nagios/nrpe.cfg

nagios-plugins:
  pkg:
    - installed

nagios-nrpe-plugin:
  pkg:
    - installed

/usr/lib/nagios/plugins/check_await:
  file.managed:
    - source: salt://nagios/templates/plugins/check_await
    - makedirs: True
    - mode: 755
    - user: root
    - group: root

{% if 'rabbitmq' in salt['grains.get']('roles', []) %}
/usr/lib/nagios/plugins/check_rabbitmq_celery_queue_length:
  file.managed:
    - source: salt://nagios/templates/plugins/check_rabbitmq_celery_queue_length
    - template: jinja
    - makedirs: True
    - mode: 755
    - user: root
    - group: root
    - defaults:
        rabbitmq_vhost: {{ rabbitmq_vhost }}

/usr/lib/nagios/plugins/check_rabbitmq_filters_queue_length:
  file.managed:
    - source: salt://nagios/templates/plugins/check_rabbitmq_filters_queue_length
    - template: jinja
    - makedirs: True
    - mode: 755
    - user: root
    - group: root
    - defaults:
        rabbitmq_vhost: {{ rabbitmq_vhost }}
{% endif %}

/usr/lib/nagios/plugins/check_zfs:
  file.managed:
    - source: salt://nagios/templates/plugins/check_zfs
    - makedirs: True
    - mode: 755
    - user: root
    - group: root

/usr/lib/nagios/plugins/check_mountpoints:
  file.managed:
    - source: salt://nagios/templates/plugins/check_mountpoints
    - makedirs: True
    - mode: 755
    - user: root
    - group: root

/etc/sudoers.d/92-nagios:
  file.managed:
    - source: salt://nagios/templates/sudoers.d/92-nagios
    - makedirs: True
    - mode: 440
    - user: root
    - group: root
